
     
      Visibility API 
      Provided by www.vision-media.ca        
      Developed by Tj Holowaychuk        
     
      ------------------------------------------------------------------------------- 
      INSTALLATION
      ------------------------------------------------------------------------------- 
      
      Simply enable the module. NOTE: this module is simply an API, do not install
      unless dependent modules are used.
      
      -------------------------------------------------------------------------------
      HOW TO USE
      ------------------------------------------------------------------------------- 

      Refer to visibility_api.module for function descriptions.
        
        PUBLIC FUNCTIONS
          visibility_api_access();
          visibility_api_form_configuration();
          


