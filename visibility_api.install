<?php

/**
 * @file 
 *   Contains install and update functions for visibility_api.module
 */

/**
 * Implementation of hook_install().
 */
function visibility_api_install() {
  drupal_install_schema('visibility_api');
}

/**
 * Implementation of hook_schema().
 */
function visibility_api_schema() {
  $schema['visibility'] = array(
    'description' => t('Stores page and role specific {visibility} settings.'),
    'fields' => array(
      'rid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique record ID.',
      ),
      'package_key' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
        'description' => 'A unique package generated key for the record.',
      ),
      'package' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The machine name of the module, theme, profile, etc.',
      ),
      'visibility' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'Flag to indicate how to show on pages. (0 = Show on all pages except listed pages, 1 = Show only on listed pages, 2 = Use custom PHP code to determine visibility.)',
      ),
      'pages' => array(
        'type' => 'text',
        'not null' => TRUE,
        'description' => 'Contents of the "Pages" block; contains either a list of paths on which to include/exclude the region or PHP code, depending on "visibility_api" setting.',
      ),
      'roles' => array(
        'type' => 'text',
        'size' => 'normal',
        'not null' => FALSE,
        'serialize' => TRUE,
        'description' => 'A serialized array of roles for this record.',
      ),
    ),
    'primary key' => array('rid'),
    'unique keys' => array(
      'kp' => array('package_key', 'package'),
    ),
  );
  return $schema;
}

/**
 * Implementation of hook_uninstall().
 */
function visibility_api_uninstall() {
  drupal_uninstall_schema('visibility_api');
  cache_clear_all('visibility:*', 'cache', TRUE);
}